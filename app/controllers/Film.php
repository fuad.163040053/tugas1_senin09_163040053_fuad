<?php 

/**
 * 
 */
class Film extends Controller
{
	public function index()
	{
		$data = array(
			'judul' => "Daftar Film",
			'fl' => $this->model('Film_model')->getAllFilm()
		);
		$this->view('templates/header', $data);
		$this->view('film/index', $data);
		$this->view('templates/footer');
	}

	public function detail($id)
	{
		$data = array(
			'judul' => "Daftar Film",
			'fl' => $this->model('Film_model')->getFilmById($id)
		);
		$this->view('templates/header', $data);
		$this->view('film/detail', $data);
		$this->view('templates/footer');
	}

	


	public function insert()
	{
		if(isset($_POST['submit'])){
			$data['gambar'] = $_POST["gambar"];
			$data['judul'] = $_POST['judul'];
			$data['genre'] = $_POST['genre'];
			$data['negara'] = $_POST['negara'];
			$data['tahun'] = $_POST['tahun'];
			$this->model('Film_model')->insert("film", $data);
			$this->index();
		}
	}


	public function hapus($id)
	{
		$this->model('Film_model')->hapus("film", $id);
		$this->index();
	}

	public function edit()
	{
		if(isset($_POST['ubah'])){
			$data['gambar'] = $_POST["gambar"];
			$data['judul'] = $_POST['judul'];
			$data['genre'] = $_POST['genre'];
			$data['negara'] = $_POST['negara'];
			$data['tahun'] = $_POST['tahun'];
			$data['id'] = $_POST['id'];
			$this->model('Film_model')->ubah("film", $data);
			$this->index();
		}
	}

	public function ubah($id)
	{
		$data = array(
			'judul' => 'Daftar Film',
			'fl' => $this->model('Film_model')->getFilmById('film', $id)
		);

		$this->view('templates/header', $data);
		$this->view('film/ubah', $data);
		$this->view('templates/footer');
	}

	public function cari()
	{
		$judul = $_POST["judul"];
		if(isset($_POST["cari"]))
		$data = array(
			'judul' => 'Daftar Film',
			'fl'=>$this->model('Film_model')->cari("film_view", $judul)
		);
				$this->view('templates/header', $data);
		$this->view('film/index', $data);
		$this->view('templates/footer');
	}
	
}