<?php 

/**
 * 
 */
class Film_model
{

	private $table = 'film';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllFilm()
	{
		
		$this->db->query("SELECT * FROM " . $this->table);
		return $this->db->resultSet();
	}

	public function getFilmById($table,$id)
	{
		$this->table =$table;
		$this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id");
		$this->db->bind("id", $id);
		return $this->db->single();
	}

	public function hapus($tabel, $id){
			$this->table = $tabel;
			$this->db->query("delete from ". $this->table .' where id=:id');
			$this->db->bind("id",$id);
			$this->db->execute();
		}
	

	public function insert($tabel, $data = []){
		$this->table = $tabel;
		$this->db->query("insert into ". $this->table ." ( gambar,genre,judul,negara,tahun)
		values(:gambar,:genre,:judul,:negara,:tahun)");
		$this->db->bind("gambar", $data["gambar"]);
		$this->db->bind("genre", $data["genre"]);
		$this->db->bind("judul", $data["judul"]);
		$this->db->bind("negara", $data["negara"]);
		$this->db->bind("tahun", $data["tahun"]);
		$this->db->execute();
	}

	public function ubah($tabel, $data = []){
		$this->table = $tabel;
		$this->db->query("update ". $this->table ." set gambar=:gambar, judul=:judul, genre=:genre, negara=:negara, tahun=:tahun where id=:id");
		$this->db->bind("gambar", $data["gambar"]);
		$this->db->bind("genre", $data["genre"]);
		$this->db->bind("judul", $data["judul"]);
		$this->db->bind("negara", $data["negara"]);
		$this->db->bind("tahun", $data["tahun"]);
		$this->db->bind("id", $data["id"]);
		$this->db->execute();
	}

	public function cari($tabel, $data){
		$this->table = $tabel;
		$this->db->query("SELECT * FROM ". $this->table ." WHERE id=:cari OR judul=:cari OR genre=:cari OR negara=:cari OR tahun=:cari");
		$this->db->bind("cari",$data);
		return $this->db->resultSet();
	}
	
}