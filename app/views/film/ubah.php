<div class="container">
	<form action="<?= BASEURL; ?>/Film/edit" method="post">
		<input type="hidden" name="id" value="<?= $data['fl']['id'] ?>">
		<div class="form-group">
			<label for="gambar"  class="col-form-label">Gambar</label>
			<input type="text" name="gambar" id="gambar" class="form-control" value="<?= $data['fl']['gambar'] ?>">
		</div>

		<div class="form-group">
			<label for="judul" class="col-form-label">Judul</label>
			<input type="text" name="judul" id="judul"  class="form-control" value="<?= $data['fl']['judul'] ?>">
		</div>
		
		<div class="form-group">
			<label for="genre" class="col-form-label">Genre</label>
			<input type="text" name="genre" id="genre"  class="form-control" value="<?= $data['fl']['genre'] ?>">
		</div>
		
		<div class="form-group">
			<label for="negara" class="col-form-label">Negara</label>
			<input type="text" name="negara" id="negara"  class="form-control" value="<?= $data['fl']['negara'] ?>">
		</div>

		<div class="form-group">
			<label for="tahun" class="col-form-label">Tahun</label>
			<input type="date" name="tahun" id="tahun"  class="form-control" value="<?= $data['fl']['tahun'] ?>">
		</div>
		
		<div class="form-group">
			<button type="submit" name="ubah" id="ubah" class="btn btn-primary">Ubah</button>
		</div>
	</form>
</div>