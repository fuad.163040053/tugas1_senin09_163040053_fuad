<div class="container mt-5">
	<div class="row">
		
		<div class="col-12">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Tambah</button>
		</div>

		<div class="col-5 mt-3">
			<form action="<?= BASEURL; ?>/Film/cari" method="post">
			  <div class="input-group mb-3">
			  	<input type="text" class="form-control"  name="judul" id="judul"  aria-describedby="button-addon2">
			  <div class="input-group-append">
			    <button class="btn btn-primary" type="sumbit" name="cari" id="button-addon2">Cari</button>
			  </div>
			</div>
			</form>





		</div>
		
		<table class="table">
			<thead>
				<tr>
					<th>Aksi</th>
					<th>Gambar</th>
					<th>Judul</th>
					<th>Genre</th>
					<th>Negara</th>
					<th>Tahun</th>
				</tr>
			</thead>

			<?php foreach ($data['fl'] as $key) : ?>	
			<tr>
				<td><a href="<?= BASEURL; ?>/Film/ubah/<?= $key['id']; ?>" class="btn badge-primary">Ubah</a>
					<a href="<?= BASEURL; ?>/Film/hapus/<?= $key['id']; ?>" class="btn badge-danger">Hapus</a></td>
				<td><img src="<?= BASEURL; ?>/img/<?= $key['gambar']  ?>"></td>
				<td><?= $key['judul'] ?></td>
				<td><?= $key['genre'] ?></td>
				<td><?= $key['negara'] ?></td>
				<td><?= $key['tahun'] ?></td>
			</tr>
			<?php endforeach; ?>
		</table>

		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
			 		<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Tambah Film</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
			 		</div>
			  		
			  		<div class="modal-body">
						<form action="<?= BASEURL; ?>/Film/insert" method="post">
				 			<div class="form-group">
								<label for="gambar"  class="col-form-label">Gambar</label>
								<input type="text" name="gambar" id="nama" class="form-control">
				  			</div>
				  			
				  			<div class="form-group">
								<label for="judul"  class="col-form-label">Judul</label>
								<input type="text" name="judul" id="judul" class="form-control">
				  			</div>
				  	
				  			<div class="form-group">
								<label for="genre" class="col-form-label">Genre</label>
								<input type="text" name="genre" id="genre"  class="form-control">
				  			</div>

				  			<div class="form-group">
								<label for="negara" class="col-form-label">Negara</label>
								<input type="text" name="negara" id="negara"  class="form-control">
				  			</div>
				  
				  			<div class="form-group">
								<label for="tahun" class="col-form-label">Tahun</label>
								<input type="date" name="tahun" id="tahun"  class="form-control">
				  			</div>
			  		
			  				<div class="modal-footer">
								<button type="submit" name="submit" class="btn btn-primary">Simpan</button>
			  				</div>
			 			</form>
				</div>
		  	</div>
		</div>
	</div>
</div>