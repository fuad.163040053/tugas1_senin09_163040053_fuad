-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2018 at 12:12 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `daftar_film`
--

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `negara` varchar(50) NOT NULL,
  `tahun` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `judul`, `gambar`, `genre`, `negara`, `tahun`) VALUES
(1, 'Avenger', 'avenger.jpg', 'Scien-Fi', 'Amareka', '2018-04-25'),
(2, 'Danur', 'danur.jpg', 'Horor', 'Indonesia', '2018-03-29'),
(3, 'Dilan', 'dilan.jpg', 'Romance', 'Indonesia', '2018-01-25'),
(4, 'Train To Busan', 'train.jpg', 'Horor', 'Korea', '2016-07-20'),
(9, 'Big Hero 6', 'big.jpg', 'Animasi', 'Amerika', '2014-02-01'),
(18, 'jjkk', 'big.jpg', 'Animasi', 'indo', '2222-02-22');

-- --------------------------------------------------------

--
-- Stand-in structure for view `film_view`
-- (See below for the actual view)
--
CREATE TABLE `film_view` (
`id` int(11)
,`gambar` varchar(50)
,`judul` varchar(50)
,`genre` varchar(50)
,`negara` varchar(50)
,`tahun` date
);

-- --------------------------------------------------------

--
-- Structure for view `film_view`
--
DROP TABLE IF EXISTS `film_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `film_view`  AS  select `film`.`id` AS `id`,`film`.`gambar` AS `gambar`,`film`.`judul` AS `judul`,`film`.`genre` AS `genre`,`film`.`negara` AS `negara`,`film`.`tahun` AS `tahun` from `film` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
